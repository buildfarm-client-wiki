Welcome to the exim-build-farm-client wiki!

The Exim Build Farm Client software is run on machines to build and test the [Exim](https://www.exim.org) MTA software.  It uses the current development tree and rebuilds/tests Exim when new commits are added to the [Exim source repository](https://github.com/Exim/exim). The result are recorded and reported back to the [Exim Build Farm](https://buildfarm.exim.org) website where developers and interested parties can review builds and tests across various distros and versions.

View the [Installation](./Installation.html) page for info on how to install the Build Farm Client.  There is an additional separate page on preparing your system to [build documentation](./BuildingDocs.html).

## Overview

In the default configuration, a full rebuild of Exim will occur when a new commit to the monitored branches occurs or every 7 days, and it will also build and run the test suite.  You can choose to have it also build the docs, but there are several extra package requirements for [building the documentation](./BuildingDocs.html).

Review your enabled features in the Makefile and make adjustments to your *build-farm.conf* to enable those features, using either the makefile_set, makefile_add, or makefile_regex.  You may also receive requests from the developers to enable specific features on your build.  Documentation for enabling these in [build-config.conf](./BuildConfigConf.html) is elsewhere.

The default build configuration will build all branches specified in a text file on the server.  Most of the time this will only be HEAD, but occasionally the developers may have an interest in building some experimental code on a different branch.  The above configuration will do all of it automatically for you.  The *run_cron* will also check the master repo at of the Exim Build Farm Client Software and update itself.

## Credits
This code is derived from the excellent [PostgreSQL Build Farm](https://www.pgbuildfarm.org/) client and server code.  Many thanks to them for a fantastic collection which serves such a useful function.
