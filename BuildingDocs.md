Running the stage which builds docs helps to test our txt, pdf, and html generation across the various versions of ghostscript and xml support that exist through all participants in the Exim BuildFarm.  It requires quite a few packages to be installed.  I'll relate my experiences getting it working on CentOS systems and hopefully this can be enough of a guide to get it working on yours.

## Getting Required Packages
1. I had to find, download, and install the xfpt (Xml From Plain Text) rpm.
The package is available directly on Debian and Ubuntu distros.
For CentOS 6, it installed with no problem.
For CentOS 5, I had to get the src rpm and rebuild it on my CentOS 5 build system: `rpm -Uvh xfpt-0.08-1.x86_64.rpm`
The above package is available for download for [CentOS 6](http://www.blafasel.at/exim/doc_rpms/) and [CentOS 5](http://downloads.mrball.net/Linux/CentOS/5/RPMS/x86_64/).
Fedora 27 does not seem to have it.
There are git source repos on [github](https://github.com/nigelm/xfpt) and also [here](https://gitlab.quatermass.co.uk/mirrors/xfpt).
2. I had to install several additional distro provided packages:
`yum install xmlto ghostscript docbook-style-xsl gc w3m`
3. I had to find, download, and compile the sdop tarball (Google found it on the debian build system).
There is a git copy of that [here](https://gitlab.quatermass.co.uk/jgh/sdop).
For CentOS 6, it was as simple as:
`./configure && make && make install`
For CentOS 5, it was more complicated because the older version of gcc didn't like the way things were being typedef'd in the png headers; I had to do:
`./configure --disable-jpeg --disable-png && make && make install`
A package is available for download for [CentOS 6](http://www.blafasel.at/exim/doc_rpms/).
The package is available directly on the Debian and Ubuntu distros.

Once that was in place, the buildfarm build system can do all the steps to make all of the documentation.
