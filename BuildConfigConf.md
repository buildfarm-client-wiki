This page describes the options in the build-config.conf.

### Required options:
* scm - For the Exim project, this is set to 'git'.
* scm_repo - Defaults to the git repo hosted at github.
* build_root - Full path to the work directory that holds the local git checkout and the temp directory where the test runs.
* animal - Set the host alias assigned to you by the BuildFarm administrators.
* secret - The secret assigned to you by the BuildFarm adminstrator.  In the words of Gandalf, "keep it secret, keep it safe."
* optional_steps - Uncomment *make-doc* and/or *test* in order to include building those stages.  Extra packages are required for building documentation, documented elsewhere.

### Enabling exim features
In order to enable exim features, you'll use the `makefile_set`, `makefile_add`, or `makefile_regex` to enable them.  Here are some samples:

    # Settings to set in Local/Makefile. These will set or override
    # previous definitions of variables.  Example:
    # LDFLAGS => '-Wall' will create LDFLAGS = '-Wall'
    makefile_set =>{
        # comment out if not using ccache
        CC               => 'ccache gcc',
        SUPPORT_TLS      => 'yes',
        USE_OPENSSL_PC   => 'openssl',
        #USE_GNUTLS       => 'yes',
        #USE_GNUTLS_PC    => 'gnutls',
        #TLS_LIBS         => '-lssl -lcrypto',
        AUTH_PLAINTEXT   => 'yes',
        LOOKUP_INCLUDE   => '-I/usr/include/mysql',
        LOOKUP_LIBS      => '-lmysqlclient -lpq',
        EXPERIMENTAL_SPF => 'yes',
        LOOKUP_LDAP      => 'yes',
        LOOKUP_INCLUDE   => '-I/usr/include/mysql',
        LOOKUP_LIBS      => '-lmysqlclient -lpq -lldap -llber',
        LDAP_LIB_TYPE    => 'OPENLDAP2',
    },
    # Settings to add to Local/Makefile. These will add to variables that
    # are already defined earlier in the Makefile.  Example:
    # LDFLAGS => '-Wall'  will create  LDFLAGS+='-Wall'
    makefile_add =>{
        # Show all warnings and errors
        CFLAGS         => '-Wall -Werror=format-security',
        # Or enable debugging flags
        #CFLAGS         => '-g -Wall',
        #LFLAGS         => '-g',
        LDFLAGS        => '-lspf2',
        LOOKUP_INCLUDE => '-I/usr/include/postgresql',
    },
    # Make inline changes to existing settings. These could easily be done with
    # makefile_set above, but this method allows for the option (in EDITME) to get
    # removed. Then this will have no further effect, where as makefile_set still would.
    makefile_regex =>[
        q(s/^# EXPERIMENTAL_CERTNAMES.*/EXPERIMENTAL_CERTNAMES=yes/),
        q(s/^# EXPERIMENTAL_TPDA.*/EXPERIMENTAL_TPDA=yes/),
        q(s/^# EXIM_PERL=/EXIM_PERL=/),
        q(s/^# LOOKUP_MYSQL=/LOOKUP_MYSQL=/),
        q(s/^# LOOKUP_PGSQL=/LOOKUP_PGSQL=/),
    ],
