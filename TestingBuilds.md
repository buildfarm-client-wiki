The installation page has a simple test process which will work most of the time to get your system up and running.  This page has more detail about running the test process to achieve various things.

The base script which handles everything is the run_build.pl script.  The commandline options are:

    $ ./run_build.pl --help
    
    usage: ./run_build.pl [options] [branch]
    
     where options are one or more of:
    
      --nosend                  = don't send results
      --nostatus                = don't set status files
      --force                   = force a build run (ignore status files)
      --from-source=/path       = use source in path, not from SCM
      or
      --from-source-clean=/path = same as --from-source, run make distclean first
      --config=/path/to/file    = alternative location for config file
      --keepall                 = keep directories if an error occurs
      --verbose[=n]             = verbosity (default 1) 2 or more = huge output.
      --quiet                   = suppress normal error message
      --test                    = short for --nosend --nostatus --verbose --force
      --skip-steps=list         = skip certain steps
      --only-steps=list         = only do certain steps, not allowed with skip-steps
                                  lists can be comma, colon, or space separated
    
    Default branch is HEAD. Usually only the --config option should be necessary.

### Valid Stages:
* configure
* make
* make-doc  (optional, off by default)
* test      (optional, off by default)

In each of the following sample command lines, remember that `--test` will force all steps and will not locally store the state of the git checkout nor the state of each of stage, nor will it submit the results to the server.

1. Most of the time, you probably just want to enable a feature and test if the build process changes.  In that case, this command is likely all that you'll want:
`./run_build.pl --test --verbose=2 --only-steps=configure,make`

2. Test mode, run just configure and make steps, and keep the directories:
`./run_build.pl --test --only-steps configure,make --keepall`

3. Test mode, debug mode, run just configure and make steps:
`./run_build.pl --test --only-steps configure,make --verbose=2`

4. Debug mode, only run configure and make if there is a new commit, submit results to server:
`./run_build.pl --only-steps configure,make --verbose=2`

5. Run all steps, only if there is a new commit, configured to also do make_test, but limit the range of tests from 1 to 9:
`./run_build.pl --override range_test_num='1 9'`

6. Just build docs, don't submit results (configure builds the *Local/Makefile*, which is required for doc building) :
`./run_build.pl --force --only-steps=configure,make-doc --nosend`

7. Just build docs, equivalent command:
`./run_build.pl --test --only-steps=configure,make-doc`

8. Just build docs, but allow it to submit results (not advised since you aren't building the actual binary) :
`./run_build.pl --force --only-steps=configure,make-doc`


### Extreme test debugging
Sometimes you need to figure out why one test is failing.  To see exactly what is happening, you can create a temporary build, don't delete it, and then manually run the test in a debug mode.  In the following example, I'll assume that I'm wanting to find why test 433 is failing

    PATH="$PATH:/sbin" ./run_build.pl --test --verbose=2 --force --only-steps=configure,make,test \
      --override range_num_tests='1 1' --keepall

The above will create a subdirectory of the format _exim.$PID_ (of the script running the build farm).  In the example above, the git checkout of the exim sources was in `$HOME/buildfarm/HEAD/exim.4689/`, so we change into the test directory and manually instantiate the runtest script, specifying which test we want it to run.

    ./runtest $HOME/buildfarm/HEAD/exim.4689/src/build-Linux-i386/exim -DEBUG 433 433

It will print out copious debug output and you can hopefully find the problem based on that output.